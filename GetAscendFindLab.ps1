Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Remove-Item "C:\Labs\FunWithFind" -Force -Recurse
New-Item "C:\Labs\FunWithFind\" -itemtype directory

$client = new-object System.Net.WebClient
$client.DownloadFile("https://gitlab.com/cjmagee/AscendFindLab/repository/archive.zip?ref=master","C:\Labs\FunWithFind\FromGit.zip")

Unzip "C:\Labs\FunWithFind\FromGit.zip" "C:\Labs\FunWithFind"