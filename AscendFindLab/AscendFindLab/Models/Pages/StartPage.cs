﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace AscendFindLab.Models.Pages
{
    [ContentType(GUID = "1315c493-4299-40cc-9a86-193bf8586e52", 
        DisplayName = "Start Page")]
    public class StartPage : PageData
    {
    }
}