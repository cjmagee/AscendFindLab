﻿using AscendFindLab.Models.Pages;
using AscendFindLab.Models.Projections;
using AscendFindLab.Steam.Models;
using EPiServer.Find;
using EPiServer.Find.Api.Facets;
using X.PagedList;

namespace AscendFindLab.Models.ViewModels
{
    public class StartPageViewModel
    {
        public StartPageViewModel(StartPage currentPage)
        {
            CurrentPage = currentPage;
        }

        public StartPage CurrentPage { get; set; }
        public int TotalResults { get; set; }
        public TermsFacet CategoriesFacet { get; set; }
        public TermsFacet GenresFacet { get; set; }
        public StaticPagedList<ISteamGame> PagedList { get; set; }
        public string BoostPrice { get; set; }
        public string BoostCategory { get; set; }
        public string BoostGenre { get; set; }

        // The actual search results
        #region Edit inside of this area

        public SearchResults<SteamGame> SearchResults { get; set; }

        #endregion

    }
}