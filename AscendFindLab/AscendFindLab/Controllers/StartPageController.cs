﻿using AscendFindLab.Business.Extensions;
using AscendFindLab.Business.Search;
using AscendFindLab.Models.Pages;
using AscendFindLab.Models.Projections;
using AscendFindLab.Models.ViewModels;
using AscendFindLab.Steam.Models;
using EPiServer.Find;
using EPiServer.Find.Framework;
using EPiServer.Find.UnifiedSearch;
using EPiServer.Web.Mvc;
using System.Linq;
using System.Web.Mvc;
using X.PagedList;

namespace AscendFindLab.Controllers
{
    public class StartPageController : PageController<StartPage>
    {
        public ActionResult Index(StartPage currentPage, SearchParameters searchParameters)
        {
            var model = new StartPageViewModel(currentPage);

            // Set the current pagination page number if it doesn't exist
            searchParameters.Page = (searchParameters.Page <= 0) ? 1 : searchParameters.Page;

            // Set the skip and take
            var resultsPerPage = 10;
            var skip = (searchParameters.Page - 1) * resultsPerPage;
            var take = resultsPerPage;

            // Set up the base query. We need all SteamGame objects and we need to set up some facets for our sidebar
            // We'll also boost the selected facets, and do a skip/take for pagination
            var query = SearchClient.Instance.Search<SteamGame>()
                .TermsFacetFor(x => x.Categories, x => x.Description, facetRequestAction: r => r.Size = 1000)
                .TermsFacetFor(x => x.Genres, x => x.Description, facetRequestAction: r => r.Size = 1000)
                .ApplySteamGameBoosting(searchParameters)
                .Skip(skip).Take(take);

            // Get the results
            #region Edit inside of this area

            // If we want the full object, we can just call GetResult() on the query and pass it into the viewmodel,
            var results = query.GetResult();

            // We could also project the results into a custom object
            // Write your code here!

            #endregion

            // Set our search results
            model.SearchResults = results;

            // Set the total results, facets, and selected facets for the view
            model.PagedList = new StaticPagedList<ISteamGame>(results.AsEnumerable(), searchParameters.Page, resultsPerPage, results.TotalMatching);
            model.TotalResults = results.TotalMatching;
            model.CategoriesFacet = results.TermsFacetFor(x => x.Categories, x => x.Description);
            model.GenresFacet = results.TermsFacetFor(x => x.Genres, x => x.Description);
            model.BoostPrice = searchParameters.BoostPrice;
            model.BoostCategory = searchParameters.BoostCategory;
            model.BoostGenre = searchParameters.BoostGenre;

            return View(model);
        }
    }
}