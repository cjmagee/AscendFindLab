﻿# Fun with Find

Episerver Find is more than just powerful search solution. In this lab we will take Find a little bit further by creating an advanced search application and learn different ways we can utilize Find in our Episerver solutions.

## Scenario 1: Projections

The solution is already configured to display results from our Find index, and the object that is indexed is called `SteamGame`. But this is a big object and it contains a lot of properties. Since we only care about a couple properties for our view, we can project the `SteamGame` object into another smaller object.

For this scenario, you will create a smaller object, called `SteamGameLite`, project our `SteamGame` properties into this new object, update the view model, and change the view to use a different partial.

### Scenario Steps

1. Create the `SteamGameLite` object, inheriting the `ISteamGame` interface.
2. Update the Find `query` variable to project the `SteamGame` properties onto `SteamGameLite`.
3. Change the `StartPageViewModel` to use the `SteamGameLite` object in the `Results` property.
4. Change the `Index.cshtml` view of the `StartPage` to use the `_SteamGameLightResults.cshtml` partial.

*Note: In our example, we are showing we can do more with the projected property values by performing a `.FriendlyTruncate(200)` on the `ShortDescription` property.*

### Scenario Code

*This is the answer! Try it yourself first!*

**/Models/Projections/SteamGameLite.cs**
```
using AscendFindLab.Steam.Models;
using System.Collections.Generic;

namespace AscendFindLab.Models.Projections
{
    public class SteamGameLite : ISteamGame
    {
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string HeaderImage { get; set; }

        public IEnumerable<SteamGameAttribute> Categories { get; set; }
        public IEnumerable<SteamGameAttribute> Genres { get; set; }
    }
}
```

**/Controllers/StartPageController.cs**
```
var results = query
    .Select(x => new SteamGameLite
    {
        ApplicationId = x.ApplicationId,
        Name = x.Name,
        ShortDescription = x.ShortDescription.FriendlyTruncate(200),
        HeaderImage = x.HeaderImage
    }).GetResult();
```

**/Models/ViewModels/StartPageViewModel.cs**
```
public SearchResults<SteamGameLite> SearchResults { get; set; }
```

**/Views/StartPage/Index.cshtml**
```
@Html.Partial("_SteamGameLiteResults", Model.SearchResults)
```

## Scenario 2: More/Related Games

Now that we can search for games and filter games down to our criteria, let's pretend we bought a game, we play it all the way through, and we're done with it, but we really liked it. Let's see if we can create a search to show games like the one you just tried.

To do this, we'll use our `SteamGameLite` projections in Scenario #1 to show a small summary of the related games on the game's detail page.

We'll need to fill out our `RelatedGames` property on the `GamePageViewModel`.

### Scenario Steps

1. Find the `RelatedGames` property on the `GamePageViewModel`
2. Find where the `GamePageViewModel` is built in the `Index` method on the `GameController`
3. Make a new request to Find to get `SteamGame` results that have similarities to the game being shown
    - Find games using the Find's `MoreLike(string)` method, which takes a single string
    - Use a concatenated list of the shown `SteamGame` Genres and Categories (check for existing concatenated properties)
    - Limit the `MoreLike()` criteria to show games at least matching 40% of the terms (`PercentTermsToMatch` from http://world.episerver.com/documentation/Items/Developers-Guide/EPiServer-Find/9/DotNET-Client-API/Searching/More-LikeRelated/)
4. Fill the `RelatedGames` property up with the results of your search
5. Edit `/Views/Game/Index.cshml` to loop through the related games and show a `SteamGameLite` summary

### Scenario Code

*This is the answer! Try it yourself first!*

**Controllers\GameController.cs**
```
if (game != null)
{
    var relatedGames = SearchClient.Instance.Search<SteamGame>()
        .MoreLike(game.CategoryAndGenreTermsJoined)
        .PercentTermsToMatch(40)
        .Take(MAX_RELATED_GAMES)
        .Select(x => new SteamGameLite
        {
            ApplicationId = x.ApplicationId,
            Name = x.Name,
            ShortDescription = x.ShortDescription.FriendlyTruncate(200),
            HeaderImage = x.HeaderImage
        })
        .GetResult();

    gameViewModel.RelatedGames = relatedGames.ToList();
}
```

**Views\Game\Index.cshtml**
```
@foreach (var relatedGame in Model.RelatedGames)
{
    @Html.Partial("Game/_SteamGameLite", relatedGame)
}
```

## Other Notes

**Episerver login**<br/>
User: admin<br/>
Pass: Password123