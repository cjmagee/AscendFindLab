﻿using AscendFindLab.Steam.Models;
using EPiServer.Find;

namespace AscendFindLab.Business.Search
{
    public static class SearchExtensions
    {
        public static ITypeSearch<SteamGame> ApplySteamGameBoosting(this ITypeSearch<SteamGame> query, SearchParameters searchParameters)
        {
            // Boost the fields based on what's selected in the sidebar
            if (!string.IsNullOrEmpty(searchParameters.BoostPrice))
            {
                if (searchParameters.BoostPrice.Equals("free", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    query = query.BoostMatching(x => x.IsFree.Match(true), 2);
                }
                else if (searchParameters.BoostPrice.Equals("notfree", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    query = query.BoostMatching(x => x.IsFree.Match(false), 2);
                }
            }

            if (!string.IsNullOrEmpty(searchParameters.BoostCategory))
            {
                query = query.BoostMatching(x => x.CategoryTerms.Match(searchParameters.BoostCategory), 2);
            }

            if (!string.IsNullOrEmpty(searchParameters.BoostGenre))
            {
                query = query.BoostMatching(x => x.GenreTerms.Match(searchParameters.BoostGenre), 2);
            }

            return query;
        }
    }
}