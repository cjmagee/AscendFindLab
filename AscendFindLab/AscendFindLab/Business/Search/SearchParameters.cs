﻿namespace AscendFindLab.Business.Search
{
    public class SearchParameters
    {
        public int Page { get; set; }

        public string BoostPrice { get; set; }
        public string BoostGenre { get; set; }
        public string BoostCategory { get; set; }
    }
}