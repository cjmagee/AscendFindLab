﻿using AscendFindLab.Business.Extensions;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace AscendFindLab.Business.Utilities
{
    public static class UrlUtility
    {
        private static Injected<UrlResolver> _urlResolver;

        public static string GetContentUrlWithAllQueryStrings(Uri url, ContentReference contentLink)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            var contentUrl = _urlResolver.Service.GetUrl(contentLink);

            return $"{contentUrl}?{queryStrings}";
        }

        public static string GetContentUrlWithRemovedQueryStrings(Uri url, ContentReference contentLink, string[] removeKeys)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            var contentUrl = _urlResolver.Service.GetUrl(contentLink);

            foreach (var key in removeKeys)
            {
                queryStrings.Remove(key);
            }

            return $"{contentUrl}?{queryStrings}";
        }

        public static string GetContentUrlWithSpecificQueryStrings(Uri url, ContentReference contentLink, string[] queryStringKeys)
        {
            var contentUrl = _urlResolver.Service.GetUrl(contentLink);

            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            if (queryStrings.Count <= 0)
            {
                return $"{contentUrl}";
            }

            var filteredQueryStrings = new NameValueCollection();

            foreach (string queryStringKey in queryStrings.Keys)
            {
                if (queryStringKeys.Contains(queryStringKey))
                {
                    filteredQueryStrings.Add(queryStringKey, queryStrings[queryStringKey]);
                }
            }

            if (filteredQueryStrings.Count <= 0)
            {
                return $"{contentUrl}";
            }

            return $"{contentUrl}?{filteredQueryStrings.ToQueryString()}";
        }

        public static bool GetQueryStringValueFromUrl(Uri url, string key, out string value)
        {
            var queryStringValue = string.Empty;
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            try
            {
                queryStringValue = queryStrings.Get(key);
                value = queryStringValue;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    return true;
                }

                return false;
            }
            catch
            {
                value = queryStringValue;
                return false;
            }
        }

        public static string AddQueryString(Uri url, string key, string value)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            queryStrings.Remove(key);

            if (!string.IsNullOrEmpty(value))
            {
                queryStrings.Add(key, value);
            }

            return $"{url.AbsolutePath}?{queryStrings}";
        }

        public static string AddQueryStrings(Uri url, Dictionary<string, string> keyValuePairs)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            foreach (var keyValuePair in keyValuePairs)
            {
                queryStrings.Remove(keyValuePair.Key);

                if (!string.IsNullOrEmpty(keyValuePair.Value))
                {
                    queryStrings.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            return $"{url.AbsolutePath}?{queryStrings}";
        }

        public static string RemoveQueryStrings(Uri url, string[] keys)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            foreach (var key in keys)
            {
                queryStrings.Remove(key);
            }

            return $"{url.AbsolutePath}?{queryStrings}";
        }

        public static string UpdateQueryString(Uri url, Dictionary<string, string> addKeys, string[] removeKeys)
        {
            var queryStrings = HttpUtility.ParseQueryString(url.Query);

            foreach (var keyValuePair in addKeys)
            {
                queryStrings.Remove(keyValuePair.Key);

                if (!string.IsNullOrEmpty(keyValuePair.Value))
                {
                    queryStrings.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            foreach (var key in removeKeys)
            {
                queryStrings.Remove(key);
            }

            return $"{url.AbsolutePath}?{queryStrings}";
        }
    }
}