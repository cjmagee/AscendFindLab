﻿using System;
using System.Linq;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Find.Framework;
using EPiServer.Find;
using EPiServer.Find.Cms;
using EPiServer.Find.Cms.Conventions;
using AscendFindLab.Steam.Models;
using EPiServer.Core;
using EPiServer.Find.ClientConventions;

namespace AscendFindLab.Business.Initializations
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class FindInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            //Add initialization logic, this method is called once after CMS has been initialized

            // For our solution, we only care about the SteamGame object, which we will manually index.
            ContentIndexer.Instance.Conventions.ForInstancesOf<IContent>().ShouldIndex(x => false);

            // The SteamGame object doesn't have a property called "Id", 
            // so we will specify the property that should act as the ID
            SearchClient.Instance.Conventions.ForInstancesOf<SteamGame>().IdIs(x => x.ApplicationId);

            // Configure the Genres and Categories properties as nested objects in the SteamGame
            SearchClient.Instance.Conventions.NestedConventions.ForType<SteamGame>().Add(x => x.Genres);
            SearchClient.Instance.Conventions.NestedConventions.ForType<SteamGame>().Add(x => x.Categories);
        }

        public void Uninitialize(InitializationEngine context)
        {
            //Add uninitialization logic
        }
    }
}