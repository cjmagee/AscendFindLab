﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AscendFindLab.Steam.Services;
using Newtonsoft.Json;

namespace AscendFindLab.SourceData
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiGames = SteamService.GetSteamGames();

            SteamService.SaveGames(apiGames);

            var steamGames = SteamService.UpdateGameDetails();
            
            var rustGame = steamGames.FirstOrDefault(x => x.ApplicationId == SteamService.RustAppId);

            if (rustGame != null)
            {
                Console.WriteLine(JsonConvert.SerializeObject(rustGame));
            }

            SteamService.SaveGames(steamGames);
        }
    }
}