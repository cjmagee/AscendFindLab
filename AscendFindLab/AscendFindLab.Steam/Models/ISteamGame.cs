﻿using System.Collections.Generic;

namespace AscendFindLab.Steam.Models
{
    public interface ISteamGame
    {
        int ApplicationId { get; set; }
        string Name { get; set; }
        string ShortDescription { get; set; }
        string HeaderImage { get; set; }

        // We need these to create the facets
        IEnumerable<SteamGameAttribute> Categories { get; set; }
        IEnumerable<SteamGameAttribute> Genres { get; set; }
    }
}