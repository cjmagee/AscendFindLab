﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AscendFindLab.Steam.Models;
using Newtonsoft.Json;

namespace AscendFindLab.Steam.Services
{
    public static class SteamService
    {
        //public string SteamGameDataFileLocation = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "AscendFindLab/App_Data/SteamGamesData_{0}.json");
        public static string SteamGameDataFileAccessLocationFormat(string letter)
        {
            return string.Format(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data/SteamGamesData_{0}.json"),letter);
        }

        public const int RustAppId = 252490;

        public static IEnumerable<SteamGame> GetSteamGames(bool refreshFromApi = false)
        {
            if (refreshFromApi)
            {
                return GetSteamGamesFromApi();
            }

            return GetSteamGamesFromFile();
        }

        public static IEnumerable<SteamGame> UpdateGameDetails()
        {
            var games = GetSteamGamesFromFile();

            foreach (var nonDetailedGame in games.Where(x => !x.HasDetails))
            {
                GetSteamGameDetails(nonDetailedGame);
            }

            return games;
        }

        private static IEnumerable<SteamGame> GetSteamGamesFromFile()
        {
            List<SteamGame> games = new List<SteamGame>();

            foreach (var letter in AlphabetLetters)
            {
                var fileLocation = SteamGameDataFileLocationForLetter(letter);

                if (!File.Exists(fileLocation))
                    continue;

                string steamGamesJson = File.ReadAllText(fileLocation);

                var gamesParsed = JsonConvert.DeserializeObject<IEnumerable<SteamGame>>(steamGamesJson);

                games.AddRange(gamesParsed);
            }

            Console.WriteLine($"Found {games.Count()} Steam Games from files");

            return games;
        }

        private static void GetSteamGameDetails(SteamGame game)
        {
            using (var httpClient = new HttpClient())
            {
                try
                {
                    var json =
                        httpClient.GetStringAsync(
                            $"http://store.steampowered.com/api/appdetails?appids={game.ApplicationId}")
                            .GetAwaiter()
                            .GetResult();

                    var steamAppDetailsResponse = JsonConvert.DeserializeObject<dynamic>(json);

                    string jsonAppDetail =
                        JsonConvert.SerializeObject(steamAppDetailsResponse[$"{game.ApplicationId}"].data);

                    try
                    {
                        var appDetails =
                            JsonConvert.DeserializeObject<SteamApiResponses.SteamAppDetailsResponse>(jsonAppDetail);

                        if (appDetails != null)
                        {
                            game.AboutGame = appDetails.about_the_game;
                            game.BackgroundImage = appDetails.background;

                            game.Categories = appDetails.categories.Select(x => new SteamGameAttribute()
                            {
                                Id = x.id,
                                Description = x.description
                            });

                            game.DetailedDescription = appDetails.detailed_description;
                            game.ShortDescription = appDetails.short_description;
                            game.Developers = appDetails.developers;
                            game.Genres = appDetails.genres.Select(x => new SteamGameAttribute()
                            {
                                Id = x.id,
                                Description = x.description
                            });

                            game.HeaderImage = appDetails.header_image;
                            game.IsFree = appDetails.is_free;
                            game.Publishers = appDetails.publishers;
                            game.Website = appDetails.website;
                            game.HasDetails = true;

                            Console.WriteLine($"Fetched Details for Game {game.ApplicationId} - {game.Name}");
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception ex)
                {

                }
            }

            Thread.Sleep
        (1000);
        }

        private static IEnumerable<SteamGame> GetSteamGamesFromApi()
        {
            List<SteamGame> steamGames = new List<SteamGame>();

            using (var httpClient = new HttpClient())
            {
                var json = httpClient.GetStringAsync("http://api.steampowered.com/ISteamApps/GetAppList/v0001/").GetAwaiter().GetResult();

                try
                {
                    var steamAppListResponse = JsonConvert.DeserializeObject<SteamApiResponses.SteamAppListResponse>(json);

                    steamGames = steamAppListResponse.applist.apps.app.Select(GetSteamGameFromAppResponse).ToList();
                }
                catch (Exception ex)
                {

                }
            }

            return steamGames;
        }

        private static int SkipGames = 3;
        private static int GameIndex = 0;

        private static SteamGame GetSteamGameFromAppResponse(SteamApiResponses.SteamAppResponse steamGameResponse)
        {
            var steamGame = new SteamGame();

            steamGame.ApplicationId = steamGameResponse.appid;
            steamGame.Name = steamGameResponse.name;

            return steamGame;
        }

        static char[] AlphabetLetters = Enumerable.Range('A', 26).Select(x => (char)x).ToArray();

        public static void SaveGames(IEnumerable<SteamGame> steamGames)
        {
            foreach (var letter in AlphabetLetters)
            {
                var letterGames = steamGames.Where(x => x.Name.StartsWith($"{letter}"));

                var fileLocation = SteamGameDataFileLocationForLetter(letter);

                if (File.Exists(fileLocation))
                    File.Delete(fileLocation);

                File.WriteAllText(fileLocation, JsonConvert.SerializeObject(letterGames));
            }
        }

        private static string SteamGameDataFileLocationForLetter(char letter)
        {
            return SteamGameDataFileAccessLocationFormat($"{letter}");
        }
    }
}
